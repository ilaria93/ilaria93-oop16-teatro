package exception;

/**
 * Eccezione lanciata al tentativo di inserimento di un username gi� presente
 * nel sistema
 * 
 * @author Ilaria Carloni
 * 
 */
public class ExceptionNomeEsistente extends Exception {

	private static final long serialVersionUID = 1L;

}
